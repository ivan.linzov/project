﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    /// <summary>
    /// Логика взаимодействия для MainPage.xaml
    /// </summary>
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void CalculateButton_Clicked(object sender, EventArgs e)
        {
            int transfers = Convert.ToInt32(Picker_transfer.SelectedItem);

            int mun_cost; //стоимость проезда на муниципальном транспорте
            int sum = 0;//итоговая сумма

            if (Check_MIR.IsToggled == true) mun_cost = 19;
            else mun_cost = 25;
            
            try
            {
                if (Check_mun_1.IsToggled == true) sum += mun_cost;
                else if (Convert.ToInt32(Entry_1.Text) == 0)
                {
                    DisplayAlert("Ошибка", "Строка 1 не введена", "И правда");
                    return;
                }
                else sum += Convert.ToInt32(Entry_1.Text);
            }
            catch { DisplayAlert("Ошибка", "Неверно введена строка 1", "Ох и правда");
                return;
            };

            try
            {
                if (transfers > 0)
                if (Check_mun_2.IsToggled == true) sum += mun_cost;
                    else if (Convert.ToInt32(Entry_2.Text) == 0)
                    {
                        DisplayAlert("Ошибка", "Строка 2 не введена", "И правда");
                        return;
                    }
                else sum += Convert.ToInt32(Entry_2.Text);
            }
            catch { DisplayAlert("Ошибка", "Неверно введена строка 2", "Ох и правда");
                return;
            };

            try
            {
                if (transfers > 1)
                if (Check_mun_3.IsToggled == true) sum += mun_cost;
                    else if (Convert.ToInt32(Entry_3.Text) == 0)
                    {
                        DisplayAlert("Ошибка", "Строка 3 не введена", "И правда");
                        return;
                    }
                else sum += Convert.ToInt32(Entry_3.Text);
            }
            catch { DisplayAlert("Ошибка", "Неверно введена строка 3", "Ох и правда");
                return;
            };

            try
            {
                if (transfers > 2)
                    
                if (Check_mun_4.IsToggled == true) sum += mun_cost;
                    else if (Convert.ToInt32(Entry_4.Text) == 0)
                    {
                        DisplayAlert("Ошибка", "Строка 4 не введена", "И правда");
                        return;
                    }
                else sum += Convert.ToInt32(Entry_4.Text);
            }
            catch { DisplayAlert("Ошибка", "Неверно введена строка 4", "Ох и правда");
                return;
            };

            sum *= 2; //учет суммы затрат в обе стороны

            try
            {
                if (Convert.ToInt32(Entry_of_count.Text) == 0)
                {
                    DisplayAlert("Ошибка", "Количество поездок в неделю не введено", "И правда");
                    return;
                }
            }
            catch
            {
                DisplayAlert("Ошибка", "Что-то не то с числом поездок в неделю", "Ох мда спасибо");
                return;
            };

            try
            {
                sum *= Convert.ToInt32(Entry_of_count.Text);//
            }
            catch
            {
                DisplayAlert("Ошибка", "Неверно введено число поездок в неделю", "Ох и правда");
                return;
            };

            
            
                if (Picker_sum.SelectedIndex == 1)
                {
                    sum *= 52;
                    DisplayAlert("Результат", "Приблизительная сумма затрат на проезд за год: " + sum.ToString(), "OK");
                }
                else if(Picker_sum.SelectedIndex == 0)
                {
                    sum *= 4;
                    DisplayAlert("Результат", "Приблизительная сумма затрат на проезд за месяц: " + sum.ToString(), "OK");
                }
                else { 
                DisplayAlert("Ошибка", "Не выбрано время, за которое необходимо посчитать сумму затрат на проезд", "Ох и правда");
                return;
                };
        }

        private void Picker_transfer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int transfers = Convert.ToInt32(Picker_transfer.SelectedItem);

            switch (transfers)
            {
                case 0:
                    Label_Entry_2.IsVisible = false;
                    Check_mun_2.IsVisible = false;
                    Entry_2.IsVisible = false;

                    Label_Entry_3.IsVisible = false;
                    Check_mun_3.IsVisible = false;
                    Entry_3.IsVisible = false;

                    Label_Entry_4.IsVisible = false;
                    Check_mun_4.IsVisible = false;
                    Entry_4.IsVisible = false;

                    this.Label_Entry_Count.SetValue(Grid.RowProperty, 5);
                    this.Entry_of_count.SetValue(Grid.RowProperty, 6);

                    this.Label_Picker_sum.SetValue(Grid.RowProperty, 7);
                    this.Picker_sum.SetValue(Grid.RowProperty, 8);
                    break;

                case 1:
                    Label_Entry_2.IsVisible = true;
                    Check_mun_2.IsVisible = true;
                    Entry_2.IsVisible = true;

                    Label_Entry_3.IsVisible = false;
                    Check_mun_3.IsVisible = false;
                    Entry_3.IsVisible = false;

                    Label_Entry_4.IsVisible = false;
                    Check_mun_4.IsVisible = false;
                    Entry_4.IsVisible = false;

                    this.Label_Entry_Count.SetValue(Grid.RowProperty, 7);
                    this.Entry_of_count.SetValue(Grid.RowProperty, 8);

                    this.Label_Picker_sum.SetValue(Grid.RowProperty, 9);
                    this.Picker_sum.SetValue(Grid.RowProperty, 10);
                    break;

                case 2:
                    Label_Entry_2.IsVisible = true;
                    Check_mun_2.IsVisible = true;
                    Entry_2.IsVisible = true;

                    Label_Entry_3.IsVisible = true;
                    Check_mun_3.IsVisible = true;
                    Entry_3.IsVisible = true;

                    Label_Entry_4.IsVisible = false;
                    Check_mun_4.IsVisible = false;
                    Entry_4.IsVisible = false;

                    this.Label_Entry_Count.SetValue(Grid.RowProperty, 9);
                    this.Entry_of_count.SetValue(Grid.RowProperty, 10);

                    this.Label_Picker_sum.SetValue(Grid.RowProperty, 11);
                    this.Picker_sum.SetValue(Grid.RowProperty, 12);
                    break;

                case 3:
                    Label_Entry_2.IsVisible = true;
                    Check_mun_2.IsVisible = true;
                    Entry_2.IsVisible = true;

                    Label_Entry_3.IsVisible = true;
                    Check_mun_3.IsVisible = true;
                    Entry_3.IsVisible = true;

                    Label_Entry_4.IsVisible = true;
                    Check_mun_4.IsVisible = true;
                    Entry_4.IsVisible = true;

                    this.Label_Entry_Count.SetValue(Grid.RowProperty, 11);
                    this.Entry_of_count.SetValue(Grid.RowProperty, 12);

                    this.Label_Picker_sum.SetValue(Grid.RowProperty, 13);
                    this.Picker_sum.SetValue(Grid.RowProperty, 14);
                    break;
            }
        }

        private void Check_mun_1_Toggled(object sender, ToggledEventArgs e)
        {
            if (Check_mun_1.IsToggled)
            {
                Entry_1.IsEnabled = false;
                if (Check_MIR.IsToggled == true) Entry_1.Text = "19";
                else Entry_1.Text = "25";
            }
            else
            {
                Entry_1.IsEnabled = true;
                Entry_1.Text = null;
            }
        }
        private void Check_mun_2_Toggled(object sender, ToggledEventArgs e)
        {
            if (Check_mun_2.IsToggled)
            {
                Entry_2.IsEnabled = false;
                if (Check_MIR.IsToggled == true) Entry_2.Text = "19";
                else Entry_2.Text = "25";
            }
            else
            {
                Entry_2.IsEnabled = true;
                Entry_2.Text = null;
            }
        }

        private void Check_mun_3_Toggled(object sender, ToggledEventArgs e)
        {
            if (Check_mun_3.IsToggled)
            {
                Entry_3.IsEnabled = false;
                if (Check_MIR.IsToggled == true) Entry_3.Text = "19";
                else Entry_3.Text = "25";
            }
            else
            {
                Entry_3.IsEnabled = true;
                Entry_3.Text = null;
            }
        }

        private void Check_mun_4_Toggled(object sender, ToggledEventArgs e)
        {
            if (Check_mun_4.IsToggled)
            {
                Entry_4.IsEnabled = false;
                if (Check_MIR.IsToggled == true) Entry_4.Text = "19";
                else Entry_4.Text = "25";
            }
            else
            {
                Entry_4.IsEnabled = true;
                Entry_4.Text = null;
            }
        }
    }
}
